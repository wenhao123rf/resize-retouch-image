import os
import cv2
import argparse

def resizeImages(path,savePath):
    # path=r'E:/123rf Data/ffhqr/'
    # savePath=r"E:/123rf Data/resized/"
    count=0
    shape = (512,512)
    if not os.path.isdir(savePath):
        os.makedirs(savePath)
    os.chdir(path)
    for file in os.listdir(path):
        if not os.path.isdir(savePath):
            os.makedirs(savePath)
        if os.path.splitext(file)[1] == '.png':
            print("Processing {} Picture Number: {}".format(file,count))
            # for pic in os.listdir(os.path.join(path,folder)):
            img = cv2.imread(file)
            resizedImg= cv2.resize(img,shape)
            name = os.path.splitext(file)[0] +'.jpg'
            cv2.imwrite(os.path.join(savePath,name),resizedImg)
            count+=1
    print("Total Image: {}".format(count))

if __name__ =='__main__':
    parser = argparse.ArgumentParser(description="Resize Images to 512x512x3")
    parser.add_argument("-src", help="Path to source of image folder")
    parser.add_argument('-dest', help='Path to save the resized folders')
    args = parser.parse_args()
    resizeImages(str(args.src),str(args.dest))

        