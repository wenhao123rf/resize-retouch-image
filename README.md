## Usage

`` python resizeImage.py -src /home/ubuntu/ffhqr -dest /home/ubuntu/resized/ ``

** Flags **

-src : source path to the image folder

-dest : destination path to save the resized image folders

## Example images of dataset

** FFHQ ** (Without Retouch)

![00000before](examples/00000before.jpg) 

![00001before](examples/00001before.jpg)

** FFHQR ** (With Retouch)

![00000after](examples/00000after.jpg) 

![00001before](examples/00001after.jpg)